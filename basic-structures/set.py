myset = set([ 1, 2, 3 ])
myset.add(4)
myset.add(3)
myset.pop()
myset.update([5, 6, 7])
myset.remove(5)
myset.discard(1000)

5 in myset

myset2 = set([ 4, 5, 7 ])

subtract = myset - myset2
intersection = myset & myset2
xor = myset ^ myset2
sum = myset | myset2
myset <= myset2
myset2 >= myset

for x in myset:
    print(x)

