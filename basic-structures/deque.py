import collections

mydeque = collections.deque([1, 2])

mydeque.append(3)
mydeque.pop()

mydeque.appendleft(4)
mydeque.popleft()


# heap
import heapq

myheap = heapq([ 4, 1, 2, 3 ])