mylist = [1, 2, 3, "a", "b", "c"]

# array
mylist.pop(3)
mylist.insert(3, "xx")
mylist[1:]
mylist[-1:]

# stack
mylist.pop()
mylist.append(1)