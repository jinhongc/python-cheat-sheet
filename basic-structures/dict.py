mydict = {
    'x': 10,
    'y': -10
}

mydict.update({
    'x': 1,
    'z': 5
})

del mydict['x']

if 'x' not in mydict:
    print("key x is not in mydict")

mydict.update({
    (10, 1): 0
})

mydict.pop((10, 1))

for k, v in mydict.items():
    print("key={}, value={}".format(k, v))

for k in list(mydict):
    print("key={}".format(k))

for v in mydict.values():
    print("value={}".format(v))

# dict from list
mydict2 = dict([ (1, 2), ( (2, 1), 3 ), ( "xx", 10 ) ])
for k, v in mydict2.items():
    print("key={}, value={}".format(k, v))